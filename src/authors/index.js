import React from 'react';
import html from 'react-inner-html';
import s from './index.css';
import config from '../../package.json';

export default class Authors extends React.PureComponent {
  constructor(props) {
    super(props);

    const url = 'https://ria.ru/_editorial_preview_/20181207/1758265289.html';
    const title = encodeURIComponent('Когда уберут снег на вашей улице');
    const description = encodeURIComponent(
      'Как быстро убирают снег на московских улицах — в инфографике Ria.ru'
    );

    this.socials = [
      {
        href: `http://www.facebook.com/sharer/sharer.php?u=${url}`,
        img: 'fb2.svg',
      },
      {
        href: `https://telegram.me/share/url?url=${url}&text=${title}`,
        img: 'tg2.svg',
      },
      {
        href: `http://vk.com/share.php?url=${url}&title=${title}&description=${description}&image=${config.publicPath}/assets/share.png`,
        img: 'vk2.svg',
      },
      {
        href: `http://twitter.com/intent/tweet?text=${description}&url=${url}`,
        img: 'tw.svg',
      },
    ];
  }

  render() {
    const { authors } = this.props;
    return (
      <section className={s.authors}>
        <div className={s.grid}>
          {authors.map((c, i) => (
            <article key={i}>
              {c.label !== undefined && (
                <h4 className={`${s.title}`} {...html(c.label)} />
              )}
              {c.type !== 'button' && (
                <h4 className={s.title} {...html(c.title)} />
              )}
              {c.type == 'button' && (
                <React.Fragment>
                  <h4
                    className={`${s.title} ${s.stitle}`}
                    {...html(c.social)}
                  />
                  <ul className={s.socials}>
                    {this.socials.map((social, j) => (
                      <a
                        key={j}
                        style={{
                          backgroundImage: `url(${require(`./../../public/assets/${social.img}`)})`,
                        }}
                        href={social.href}
                        className={s.social}
                        target={'_blank'}
                        rel="noreferrer"
                      />
                    ))}
                  </ul>
                </React.Fragment>
              )}
              {c.list.map((item, j) => (
                <dl key={j} className={s.row}>
                  <dt className={s.label} {...html(item.label)} />
                  <dd className={s.value} {...html(item.value)} />
                </dl>
              ))}
            </article>
          ))}
        </div>
      </section>
    );
  }
}

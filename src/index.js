import polyfills from './polyfills';

/*
 ** грузим общие стили до компонентов,
 ** чтобы не было проблем со specificity
 */
import s from './style/reset.css';
import 'leaflet/dist/leaflet.css';
import './style/preloader.css';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import pubPath from './utils/pub-path';
import config from '../package.json';

const curScript = (function() {
  const scripts = Array.from(document.getElementsByTagName('script'));
  const injectName = `${config.publicPath}dist/inject.js`;
  const injectScripts = scripts.filter(x => {
    const src = x.getAttribute('src');
    return src && src.includes(injectName);
  });
  const script = injectScripts[injectScripts.length - 1];
  return script == null ? document.currentScript : script;
})();

const lang = curScript.getAttribute('data-lang') || 'ru';

function main() {
  polyfills.then(init);
}

function init() {
  const container = createContainerBefore(curScript);
  fetch(pubPath(`data/${lang}.json`))
    .then(res => res.json())
    .then(createProps)
    .then(props => {
      ReactDOM.render(<App {...props} />, container);
    });
}

function createProps(data) {
  // обрабатываем данные, если нужно
  // и возвращаем пропсы
  data.geojson = data.geojson
    .map(geo => {
      const LatLon = geo.geometry.coordinates.map(i => {
        if (i.length > 1) {
          i = i.map(j => {
            if (j.length) {
              j = j.reverse();
            }
            return j;
          });
        }
        return i;
      });
      geo.geometry.coordinates = LatLon;
      return geo;
    })
    .sort((x, y) => {
      const first = x.properties.RenamedSt
        ? x.properties.RenamedSt.toUpperCase()
        : '';
      const second = y.properties.RenamedSt
        ? y.properties.RenamedSt.toUpperCase()
        : '';
      return first > second ? 1 : first < second ? -1 : 0;
    });
  return data;
}

function createContainerBefore(node) {
  const div = document.createElement('div');
  // eslint-disable-next-line react/no-render-return-value
  const preloader = ReactDOM.render(
    <div className="ria-infographics-container_w">
      <div className="ria-infographics-container">
        <div className="ria-infographics-block1">
          <div className="ria-infographics-lineLong" />
          <div className="ria-infographics-lineLong" />
          <div className="ria-infographics-lineLong" />
          <div className="ria-infographics-lineLong" />
          <div className="ria-infographics-lineLong" />
          <div className="ria-infographics-lineLong" />
          <div className="ria-infographics-lineLong" />
          <div className="ria-infographics-lineLong" />
        </div>
        <div className="ria-infographics-block2">
          <svg viewBox="0 0 224 227">
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M112 0C50.1441 0 0 50.7028 0 113.248C0 175.793 50.1441 226.496 112 226.496C173.856 226.496 224 175.793 224 113.248C224 97.1583 220.682 81.8523 214.699 67.9916L112 115.657V0Z"
              fill="#CBCBCB"
            />
          </svg>
        </div>
        <div className="ria-infographics-block3">
          <div className="ria-infographics-col" />
          <div className="ria-infographics-col" />
          <div className="ria-infographics-col" />
          <div className="ria-infographics-col" />
        </div>
        <div className="ria-infographics-block4">
          <div className="ria-infographics-lineShort" />
          <div className="ria-infographics-lineShort" />
          <div className="ria-infographics-lineShort" />
          <div className="ria-infographics-lineShort" />
          <div className="ria-infographics-lineShort" />
          <div className="ria-infographics-lineShort" />
          <div className="ria-infographics-lineShort" />
        </div>
      </div>
    </div>,
    div
  );
  div.appendChild(preloader);

  const container = node.parentNode.insertBefore(div, node);

  container.classList.add(s['ig-root']);

  return container;
}

main();

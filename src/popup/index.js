import s from './index.css';

export default function popupFunc(street, days, label, amounts, table) {

  const tableStr = table
    .map((row, i) => {
      const num = days[i][street.properties.Category];
      const amount = num > 4 ? amounts[1] : amounts[0];
      const value = row.template
        .replace('$num', num)
        .replace('$amount', amount);
      return `<li class='${s.row}'>
              ${i == 0 ? `<div class="${s.snowflake}"></div>` : ''}<p class='${
        s.label
      }'>${row.label}</p>
              ${i == 0 ? `<div class="${s.cleaner}"></div>` : ''}<p class='${
        s.value
      }'>${value}</p>
            </li>`;
    })
    .join('');
  return `
    <section class='${s.popup}'>
      <h3 class='${s.title}'>${
    street.properties.RenamedSt ? street.properties.RenamedSt : ''
  }</h3>
    </section>
  `;
}

{/*
       <section class='${s.description}'>
        <p class='${s.subtitle}'>${label}</p>
        <ul class='${s.table}'>
          ${tableStr}
        </ul>
        </section>
*/}

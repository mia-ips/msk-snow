import React from 'react';
import $ from 'react-inner-html';
import s from './index.css';
import cx from 'classnames';

export default class Search extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      input: '',
      list: [],
      show: false,
      selected: undefined,
      hovered: undefined,
    };

    this.scrolled = false;

    this.onChange = this.onChange.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.toggleShow = this.toggleShow.bind(this);
    this.onMouseOver = this.onMouseOver.bind(this);
    this.onMouseOut = this.onMouseOut.bind(this);
    this.onClick = this.onClick.bind(this);
  }

  componentDidMount() {
    window.addEventListener('keydown', this.onKeyPress);
    // window.addEventListener('click', this.onClick);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log(this.props.selectedStreet);
    console.log(this.state);

    if(this.props.selectedStreet!=prevProps.selectedStreet){
      let newState;
      if(this.props.selectedStreet!=-1){
        let str = this.props.geojson.find(x => x.properties.id === this.props.selectedStreet)
        newState = {
          show: true,
          list: [],
          input: str.properties.RenamedSt,
        };
        
      }else{
        newState = {
          show: false,
          list: [],
          input: ``,
        };

      }
      this.setState(newState);
    }
  }

  onClick(e) {
    const isDropDownList =
      this.search == e.target ||
      this.search.querySelectorAll(`[class~='${e.target.classList.value}']`)
        .length;
    if (!isDropDownList && this.state.show) {
      this.setState({
        show: !this.state.show,
      });
    }
  }

  onChange(e) {
    console.log(`onChange`)
    this.setState(
      {
        input: e.target.value,
        show: true,
      },
      () => this.find()
    );
  }

  onMouseOver(item) {
    if (this.scrolled) {
      this.scrolled = false;
      return;
    }
    this.setState({
      hovered: item,
    });
  }

  onMouseOut() {
    this.setState({
      hovered: undefined,
    });
  }

  onKeyPress(e) {
    let hovered = this.state.list.indexOf(this.state.hovered) || 0;
    switch (e.which) {
      case 40: {
        hovered = hovered + 1 > this.state.list.length - 1 ? 0 : hovered + 1;
        break;
      }
      case 38: {
        hovered = hovered - 1 < 0 ? this.state.list.length - 1 : hovered - 1;
        break;
      }
      case 13: {
        if (this.state.hovered !== undefined && this.state.list.length != 0) {
          this.scrolled = false;
          this.onSelect(undefined, this.state.list[hovered]);
        }
        break;
      }
      default:
        return;
    }
    const item = this.state.list[hovered];
    if (item) {
      // const input = this.state.list[hovered].properties.RenamedSt;
      this.setState(
        {
          // input,
          hovered: item,
        },
        () => {
          const option = this.list.querySelector(
            `[datavalue='${item.properties.RenamedSt}']`
          );
          if (option) {
            const listHeight = this.list.offsetHeight;
            const listScrollTop = this.list.scrollTop;
            const optionOffsetTop = option.offsetTop;
            const optionHeight = option.offsetHeight;
            const needToScroll =
              optionOffsetTop + optionHeight - listHeight - listScrollTop;
            if (needToScroll) {
              this.scrolled = true;
              this.list.scrollTop = listScrollTop + needToScroll;
            }
          }
        }
      );
    }
  }

  onSelect(e, item) {
    if (e) e.stopPropagation();
    this.setState(
      {
        input: item.properties.RenamedSt,
        selected: item,
      },
      () => {
        setTimeout(() => {
          this.setState(
            {
              list: [],
              show: true,
            },
            () => this.props.onSelectStreet(item)
          );
        }, 300);
      }
    );
  }

  toggleShow(e) {
    

    if (this.state.show){

      e.stopPropagation();
    // let newState = {
    //   show: !this.state.show,
    // };
    // if (this.state.show) {
    //   newState.list = [];
    // } else {
    //   newState.input = '';
    //   this.input.selectionStart = this.input.selectionEnd = this.input.value.length;
    //   this.input.focus();
    // }


      let newState = {
        show: false,
        list: [],
        input: ''
      };
      this.input.selectionStart = this.input.selectionEnd = this.input.value.length;
      this.input.focus();

      this.setState(newState);

      this.setState(
        newState,
        () => this.props.onUnselectStreet()
      );

    }

  }

  find() {
    let found = [];
    if (this.state.input.length >= 2) {
      found = this.props.geojson.filter(feature => {
        let arr = this.state.input.split(` `);
        let res = true;
        arr.forEach(word => {
          let fnd = feature.properties.RenamedSt.toUpperCase().includes(
            word.toUpperCase()
          )
          res = res && fnd
        });
        return res
      }

      );
    }

    if (found.length < 200)
      this.setState({
        list: found,
      });
  }

  render() {
    const searchCx = () =>
      cx({
        [`${s.btn}`]: true,
        [`${s.close}`]: this.state.show,
      });
    const stringCx = () =>
      cx({
        [`${s.string}`]: true,
        [`${s.shown}`]: true,
      });
    const itemCx = (i, item) =>
      cx({
        [`${s.item}`]: true,
        [`${s.hovered}`]:
          this.state.hovered &&
          item.properties.id == this.state.hovered.properties.id,
        [`${s.selected}`]:
          this.state.selected &&
          item.properties.id == this.state.selected.properties.id,
      });


    return (
      <section ref={node => (this.search = node)} className={s.search}>
        <section className={stringCx()}>
          <input
            ref={node => (this.input = node)}
            className={s.input}
            placeholder={this.props.placeholder}
            type={'text'}
            value={this.state.input}
            onChange={this.onChange}
            // onKeyPress={this.onKeyPress}
            autoFocus
          />
          <ul ref={node => (this.list = node)} className={s.list}>
            {this.state.list.map((item, i) => (
              <li
                className={itemCx(i, item)}
                key={i}
                {...$(item.properties.RenamedSt)}
                datavalue={item.properties.RenamedSt}
                onClick={e => this.onSelect(e, item)}
                onMouseOver={() => this.onMouseOver(item)}
                // onMouseOut={() => this.onMouseOut(item)}
              />
            ))}
          </ul>
        </section>
        


        <div className={searchCx()} onClick={e => this.toggleShow(e)}>
          <div className={s.cube}>
{/*
            <div className={s.find} />
*/}
            <div className={s.cross} />
          </div>
        </div>

        {/* <div
          className={searchCx()}
          onClick={e => this.toggleShow(e)}
          {...$(require(`!!raw-loader!./../../../public/assets/search.svg`))}
        /> */}
      </section>
    );
  }
}

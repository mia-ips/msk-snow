import 'whatwg-fetch';
import 'core-js/fn/promise';
import 'core-js/fn/array/from';
import 'core-js/fn/array/filter';
import 'core-js/fn/string/includes';

import config from '../package.json';

const currentScript = (function() {
  const scripts = Array.from(document.getElementsByTagName('script'));
  const injectName = `${config.publicUrl}dist/inject.js`;
  const O = scripts.filter(x => {
    const src = x.getAttribute('src');
    return src && src.includes(injectName);
  });
  const out = O[O.length - 1];
  return out == null ? document.currentScript : out;
})();

function createContainerBefore(node) {
  const container = node.parentNode.insertBefore(
    document.createElement('div'),
    node
  );

  return container;
}

const node = createContainerBefore(currentScript);

fetch(`${config.publicPath}/widget-inject.html`)
  .then(x => x.text())
  .then(embedCode => {
    insertAndExecute(node, embedCode);
  });

function insertAndExecute(elem, text) {
  elem.innerHTML = text;

  const scripts = [].slice.call(elem.querySelectorAll('script'));
  let execIndex = 0;

  function executeSingleScript() {
    const elem = scripts[execIndex];

    if (!elem) {
      return;
    }

    if (elem.hasAttribute('src')) {
      loadScript(elem, continueExecution);
    } else {
      evalScript(elem, continueExecution);
    }
  }

  function continueExecution(err) {
    if (err) {
      throw err;
    }

    scripts[execIndex].parentNode.removeChild(scripts[execIndex]);
    execIndex += 1;

    executeSingleScript();
  }

  executeSingleScript();
}

function loadScript(elem, cb) {
  const script = document.createElement('script');
  const src = elem.getAttribute('src');

  function onError(err) {
    cb(err);
  }

  function onLoad() {
    cb(null);
  }

  script.onerror = onError;
  script.onload = onLoad;
  script.src = src;
  elem.parentNode.insertBefore(script, elem);
}

function evalScript(elem, cb) {
  const script = document.createElement('script');
  const code = elem.text || elem.textContent || elem.innerHTML || '';

  script.appendChild(document.createTextNode(code));
  elem.parentNode.insertBefore(script, elem);
  cb(null);
}

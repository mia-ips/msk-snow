export const DIST_PATH = __webpack_public_path__; // eslint-disable-line

export default function path(str) {
  return DIST_PATH + '../' + str;
}

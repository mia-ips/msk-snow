const fs = require('fs');
const path = require('path');

const dataPath = path.resolve(__dirname, `./../../public/data/coords.1.json`);
let dataObjAll = JSON.parse(fs.readFileSync(dataPath, 'utf8'));

const filteredPath = path.resolve(
  __dirname,
  `./../../public/data/filtered.json`
);
let filtered = JSON.parse(fs.readFileSync(filteredPath, 'utf8'));

const resultPath = path.resolve(
  __dirname,
  `./../../public/data/coordsMoscow.json`
);
// let oldData = JSON.parse(fs.readFileSync(resultPath, 'utf8'));
let oldData = dataObjAll
  .map(item => {
    const category = filtered.find(i => i['Наименование ОДХ'] == item.street)[
      'Категория'
    ];
    let mdprop = item.coord.metaDataProperty.GeocoderMetaData;
    let ditem = {
      street: item.street,
      category: category,
      precision: mdprop.precision,
      kind: mdprop.kind,
      adress: mdprop.Address.formatted,
      Point: {
        lat: item.coord.Point.lat,
        lon: item.coord.Point.lon,
      },
    };
    return ditem;
  })
  .filter(i => i.category.match(/8/i) == null);

const result = JSON.stringify(oldData, null, 2);
fs.writeFileSync(resultPath, result);

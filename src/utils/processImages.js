const fs = require('fs');
const path = require('path');
const png = require('pngjs').PNG;

let z = 15;
// let x = 304;
// let y = 157;
// const resultFolder = path.resolve(
//   __dirname,
//   `./../../public/tilesB/`
// );
// const resultFolderZ = path.resolve(
//   __dirname,
//   `./../../public/tilesB/${z}`
// );
// const resultFolderX = path.resolve(
//   __dirname,
//   `./../../public/tilesB/${z}/${x}/`
// );
const resultPath = path.resolve(__dirname, `./../../public/errors.json`);
const lutPath = path.resolve(__dirname, `./../../public/blue-lut.png`);

let errors = [];

let lutFile = fs.readFileSync(lutPath);
let lutPNG = png.sync.read(lutFile);
const lData = lutPNG.data;
const lw = lutPNG.width;

for (let x = 19617; x <= 19623; x++) {
  for (let y = 10144; y <= 10167; y++) {
    const imagePath = path.resolve(
      __dirname,
      `./../../public/tiles/${z}/${x}/${y}.png`
    );
    try {
      const imgFile = fs.readFileSync(imagePath);
      let imgPNG = png.sync.read(imgFile);
      let iData = imgPNG.data;
      for (var i = 0, l = iData.length; i < l; i += 4) {
        var r, g, b;

        r = Math.floor(iData[i] / 4);
        g = Math.floor(iData[i + 1] / 4);
        b = Math.floor(iData[i + 2] / 4);

        var lutX, lutY, lutIndex;

        lutX = (b % 8) * 64 + r;
        lutY = Math.floor(b / 8) * 64 + g;
        lutIndex = (lutY * lw + lutX) * 4;

        var lutR, lutG, lutB;

        lutR = lData[lutIndex];
        lutG = lData[lutIndex + 1];
        lutB = lData[lutIndex + 2];

        iData[i] = lutR;
        iData[i + 1] = lutG;
        iData[i + 2] = lutB;
      }
      // console.log(x, y, z);
      fs.writeFileSync(imagePath, png.sync.write(imgPNG));
    } catch (err) {
      errors.push({
        error: err,
        file: `${z} ${x} ${y}`,
      });
    }
  }
}

fs.writeFileSync(resultPath, JSON.stringify(errors, null, 2));

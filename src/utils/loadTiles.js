// const fs = require('fs');
// const path = require('path');
const cmd = require('node-cmd');

const layers = {
  // 9: [[304, 314], [157, 162]],
  // 10: [[613, 625], [315, 324]],
  // 11: [[1225, 1251], [630, 648]],
  // 12: [[2451, 2498], [1261, 1298]],
  // 13: [[4903, 4996], [2523, 2595]],
  // 14: [[9810, 9985], [5050, 5188]], //9810, 9985
  15: [[19619, 19619], [10328, 10375]], //[19617, 19985], [10098, 10378]
  // 16: [[39242, 39967], [20205, 20755]],
  // 17: [[78486, 79928], [40411, 41508]],//78486-79000
  // 18: [[156975, 159855], [80826, 83010]],
};

const requestImage = (x, y, z, maxy) => {
  return new Promise((res, rej) => {
    cmd.get(
      `curl 'http://c.tiles.wmflabs.org/bw-mapnik/${z}/${x}/${y}.png' > '/Users/svetlanakostyukova/Desktop/Repos/snow-blowers/public/tiles/${z}/${x}/${y}.png'`,
      err => {
        if (!err) {
          y++;
          res(y);
        }
        if (err) {
          // console.log(err, 'request image');
          rej();
        }
      }
    );
  }).then(y => {
    if (y <= maxy) {
      return requestImage(x, y, z, maxy);
    } else return;
  });
};

const nextLayer = (x, y, z, maxx, maxy) => {
  // console.log(x, y, z);
  cmd.get(
    `mkdir '/Users/svetlanakostyukova/Desktop/Repos/snow-blowers/public/tiles/${z}/${x}'`
  );
  requestImage(x, y, z, maxy).then(() => {
    // console.log('end');
    x++;
    if (x <= maxx) {
      nextLayer(x, y, z, maxx, maxy);
    }
  });
  // .catch(err => {
  //   // console.log(err, x, y, 'next layer');
  // });
};

Object.keys(layers).map(z => {
  cmd.get(
    `mkdir '/Users/svetlanakostyukova/Desktop/Repos/snow-blowers/public/tiles/${z}'`
  );
  const minx = layers[z][0][0];
  const miny = layers[z][1][0];
  const maxx = layers[z][0][1];
  const maxy = layers[z][1][1];
  nextLayer(minx, miny, z, maxx, maxy);
});

const fs = require('fs');
const path = require('path');

const dataPath = path.resolve(
  __dirname,
  `./../../public/data/coordsMoscow.json`
);
let dataObjAll = JSON.parse(fs.readFileSync(dataPath, 'utf8'));

const filteredPath = path.resolve(
  __dirname,
  `./../../public/data/filtered.json`
);
let filtered = JSON.parse(fs.readFileSync(filteredPath, 'utf8'));

const resultPath = path.resolve(
  __dirname,
  `./../../public/data/equalsMoscow.json`
);
const resultPath2 = path.resolve(
  __dirname,
  `./../../public/data/equalsMoscowCount.json`
);
const normalPath = path.resolve(
  __dirname,
  `./../../public/data/normalMoscow.json`
);

const equals = dataObjAll.slice(0).reduce((acc, curr) => {
  const point = curr.Point.lat + ' ' + curr.Point.lon;
  const category = filtered.find(i => i['Наименование ОДХ'] == curr.street)[
    'Категория'
  ];
  const item = {
    street: curr.street,
    category: category,
  };
  if (!acc[point]) {
    acc[point] = [item];
  } else {
    acc[point].push(item);
  }
  return acc;
}, {});

const normal = Object.keys(equals)
  .filter(key => equals[key].length == 1)
  .map(key => {
    const item = equals[key][0].street;
    const coord = dataObjAll.find(i => i.street == item);
    return coord;
  });

const equalsTable = Object.keys(equals)
  .filter(key => equals[key].length > 1)
  .reduce((acc, item) => {
    return acc.concat(equals[item]);
  }, []);
// .map(key => {
//   return {
//     point: key,
//     coincidences: equals[key].length,
//     streets: equals[key].map(i => i.street).join('; '),
//   };
// });

const result = JSON.stringify(equals, null, 2);
fs.writeFileSync(resultPath, result);

fs.writeFileSync(resultPath2, JSON.stringify(equalsTable, null, 2));
fs.writeFileSync(normalPath, JSON.stringify(normal, null, 2));
